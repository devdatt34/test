import { Router, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthguardServiceService {
  constructor(private router: Router) { }

  get isLoggedIn(): boolean {
    let authToken = sessionStorage.getItem('AccessToken');
    return (authToken !== null) ? true : false;
  }

  doLogout() {
    let removeToken = sessionStorage.removeItem('AccessToken');
    let removeToken1 = sessionStorage.removeItem("RefreshToken");
    let removeToken2 = sessionStorage.removeItem("IdToken");
    if (removeToken == null && removeToken1 == null && removeToken2 == null) {
      this.router.navigate(['login']);
    }
  }
}

