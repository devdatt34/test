import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { UserCrudComponent } from './user-crud/user-crud.component';
import { ExampleFormComponent } from './example-form/example-form.component';
import { AddeditComponent } from './addedit/addedit.component';
import { CrudwithsessionComponent } from './crudwithsession/crudwithsession.component';
import { RXJSComponent } from './rxjs/rxjs.component';
import { ArrayAndStringManipulationComponent } from './array-and-string-manipulation/array-and-string-manipulation.component';
import { AuthenticationGuard } from './auth-guard/authentication.guard';
import { NovelListComponent } from './novel-list/novel-list.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { NovelDataComponent } from './novel-data/novel-data.component';
import { CrudComponent } from './crud/crud.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthenticationGuard] },
  {
    path: 'dashboard',
    children: [
      { path: 'profile', component: ProfileComponent },
      { path: 'crud', component: CrudComponent },
      { path: 'novel-data', component: NovelDataComponent },
      { path: 'learn-array-string', component: ArrayAndStringManipulationComponent },
      { path: 'rxjs', component: RXJSComponent },
      { path: 'crudWithSession', component: CrudwithsessionComponent },
      { path: 'addedit', component: AddeditComponent },
      { path: 'exampleForm', component: ExampleFormComponent },
      {
        path: 'usercrud', component: UserCrudComponent
      },
      { path: 'adminprofile', component: AdminProfileComponent }], canActivate: [AuthenticationGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
