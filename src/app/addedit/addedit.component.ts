import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-addedit',
  templateUrl: './addedit.component.html',
  styleUrls: ['./addedit.component.scss']
})
export class AddeditComponent implements OnInit {
  addEditForm: FormGroup;
  @Input() editIndex: any;
  @Input() user: any;
  @Input() flag: any;
  @Output() newItemEvent = new EventEmitter<Object>();
  constructor(private formBuilder: FormBuilder) {
    this.addEditForm = formBuilder.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required)
    });
  }

  ngOnInit(): void {
    this.addEditForm.patchValue({
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      city: this.user.city
    });
    console.log(this.addEditForm.controls.city.value);

  }
  postTable() {
    if (this.flag === 0) {
      this.addNewItem();
    } else if (this.flag === 1) {
      this.addNewRow();
    }
  }
  addNewItem() {
    let obj = {
      "firstName": this.addEditForm.controls.firstName.value,
      "lastName": this.addEditForm.controls.lastName.value,
      "city": this.addEditForm.controls.city.value
    }
    this.newItemEvent.emit(obj);
  }
  addNewRow() {
    let obj = {
      "firstName": this.addEditForm.controls.firstName.value,
      "lastName": this.addEditForm.controls.lastName.value,
      "city": this.addEditForm.controls.city.value
    }
    this.newItemEvent.emit(obj);
  }


}
