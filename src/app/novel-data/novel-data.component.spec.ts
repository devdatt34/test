import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NovelDataComponent } from './novel-data.component';

describe('NovelDataComponent', () => {
  let component: NovelDataComponent;
  let fixture: ComponentFixture<NovelDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NovelDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NovelDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
