import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-novel-data',
  templateUrl: './novel-data.component.html',
  styleUrls: ['./novel-data.component.scss']
})
export class NovelDataComponent implements OnInit {
  items = [];

  constructor() { }

  ngOnInit(): void {
  }

  addNewItem(newItem: string) {
    this.items.push(newItem);
  }
}
