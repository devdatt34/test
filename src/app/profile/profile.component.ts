import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'email', message: 'please enter a valid email address.' }
    ],

    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' }
    ]
  }


  constructor(private formBuilder: FormBuilder, private router: Router) {
    //sessionStorage.setItem('loginData', JSON.stringify(this.loginData));
    this.profileForm = formBuilder.group({
      fullName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])),
      city: new FormControl('', Validators.required)
    })
  }


  ngOnInit(): void {
    let emailId = sessionStorage.getItem('LoginUserMailId');
    let pass = sessionStorage.getItem('LoginUserPassword');
    this.profileForm.patchValue({
      email: emailId,
      password: pass
    })
  }

  postProfile() {
    let login = JSON.parse(sessionStorage.getItem('loginData'));
    login.forEach(val => {
      if (val.email === sessionStorage.getItem('LoginUserMailId')) {
        val.fullName = this.profileForm.value.fullName;
        val.email = this.profileForm.value.email;
        val.pass = this.profileForm.value.password;
        val.city = this.profileForm.value.city;
      }
    });
    sessionStorage.setItem('loginData', JSON.stringify(login));
    this.router.navigate(['dashboard/novel-data']);
  }

}
