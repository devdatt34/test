import { Component, OnInit } from '@angular/core';
import { interval, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { catchError, filter, map, retry, take } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss']
})
export class RXJSComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  interval() {
    const numbers = interval(1000);
    numbers.subscribe(value => console.log("Subscriber: " + value));
  }

  take() {
    // We pass to that operator the number of items we want to take from the first stream. 
    // It creates a second stream and only pushes onto it the number of items we’ve requested, like so: 
    const numbers = interval(1000);
    const takeThree = numbers.pipe(take(3));
    takeThree.subscribe(value => console.log("Subscriber: " + value));
  }

  pipe() {
    //  this allows us to chain operators together.
    const numbers = interval(1000);
    const takeThree = numbers.pipe(
      take(3),
      map((v) => Date.now())
    );
    takeThree.subscribe(value => console.log("Subscriber: " + value));

    // another way
    interval(1000)
      .pipe(
        take(3),
        map(v => Date.now())
      )
      .subscribe(value => console.log("Subscriber: " + value));

  }

  filter() {
    const squareOdd =
      of(1, 2, 3, 4, 5)
        .pipe(
          filter(n => n % 2 !== 0),
          map(n => n * n)
        );

    // Subscribe to get values
    squareOdd.subscribe(x => console.log(x));
  }

  catchError(){
    const apiData = ajax('/api/data').pipe(
      map((res: any) => {
        if (!res.response) {
          throw new Error('Value expected!');
        }
        return res.response;
      }),
      catchError(err => of([]))
    );
    
    apiData.subscribe({
      next(x) { console.log('data: ', x); },
      error(err) { console.log('errors already caught... will not run'); }
    });
  }

  retry(){
    const apiData = ajax('/api/data').pipe(
      map((res: any) => {
        if (!res.response) {
          console.log('Error occurred.');
          throw new Error('Value expected!');
        }
        return res.response;
      }),
      retry(3), // Retry up to 3 times before failing
      catchError(err => of([]))
    );
    
    apiData.subscribe({
      next(x) { console.log('data: ', x); },
      error(err) { console.log('errors already caught... will not run'); }
    });
  }
}
