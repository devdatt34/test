
import { environment } from './../../environments/environment';
import { from } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router, CanActivate } from '@angular/router';
import { CrudServiceService } from '../services/crud-service.service';
import { EncrDecrService } from '../services/encr-decr.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  emailId = '';
  pword = '';
  /*loginData = [{ 'email': 'test@test.com', 'pass': '12345678' },
  { 'email': 'john@test.com', 'pass': '123456' },
  { 'email': 'dev@test.com', 'pass': '123456' },
  { 'email': 'mary@test.com', 'pass': '123456' }];*/

  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'email', message: 'please enter a valid email address.' }
    ],

    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' }
    ]
  }

  constructor(private formBuilder: FormBuilder, private router: Router, private _globalService: CrudServiceService, private _Encr_Decr: EncrDecrService) {
    //sessionStorage.setItem('loginData', JSON.stringify(this.loginData));
    this.loginForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ]))

    })
  }


  ngOnInit(): void {
    sessionStorage.setItem('isLoggedIn', JSON.stringify(false));
    sessionStorage.setItem('LoginUserMailId', this.emailId);
    sessionStorage.setItem('LoginUserPassword', this.pword);
  }

  /*postData() {
    if (this.userValidate(this.loginForm.value.email, this.loginForm.value.password)) {
      this.router.navigate(['dashboard']);
      alert("Login successsfully");
      sessionStorage.setItem('isLoggedIn', JSON.stringify(true));
    } else {
      this.router.navigate(['']);
      sessionStorage.setItem('isLoggedIn', JSON.stringify(false));
      alert("Login Failed");
    }
  }*/

  async Login() {
    if (this.loginForm.valid) {
      let Obj = {
        "clientId": environment.clientId,
        "userpool": environment.poolid,
        "username": this.loginForm.controls.email.value.toLowerCase(),
        "password": this._Encr_Decr.set(this.loginForm.controls.password.value)
      };
      let url = environment.base_url + environment.api_url.login;
      let method = 'post';
      let res = await this._globalService.globalAPICall(Obj, method, url).toPromise();
      let body = JSON.parse(res.body);

      let idToken = body.output.AuthenticationResult.IdToken;
      let accessToken = body.output.AuthenticationResult.AccessToken;
      let refreshToken = body.output.AuthenticationResult.RefreshToken;
      sessionStorage.setItem("IdToken", idToken);
      sessionStorage.setItem("AccessToken", accessToken);
      sessionStorage.setItem("RefreshToken", refreshToken);
      if (JSON.parse(res.statusCode) === 200) {
        this.router.navigate(['dashboard']);
        alert("Login successsfully");
        sessionStorage.setItem('isLoggedIn', JSON.stringify(true));
      } else {
        this.router.navigate(['']);
        sessionStorage.setItem('isLoggedIn', JSON.stringify(false));
        alert("Login Failed");
      }
    }
  }

  /*isTrue: boolean = false;
  userValidate(email, password) {
    JSON.parse(sessionStorage.getItem('loginData')).forEach(val => {
      if (val.email == email && val.pass == password) {
        this.isTrue = true;
        sessionStorage.setItem('isLoggedIn', JSON.stringify(this.isTrue));
        this.emailId = email;
        this.pword = password;
        sessionStorage.setItem('LoginUserMailId', this.emailId);
        sessionStorage.setItem('LoginUserPassword', this.pword);
      }
    });
    return this.isTrue;
  }*/
}
