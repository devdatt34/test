import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'custom'
})
export class CustomPipe implements PipeTransform {
  //let suppose we are using it for square
  transform(value: number): number {
    return value * value;
  }

}
