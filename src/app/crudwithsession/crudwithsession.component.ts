import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-crudwithsession',
  templateUrl: './crudwithsession.component.html',
  styleUrls: ['./crudwithsession.component.scss']
})
export class CrudwithsessionComponent implements OnInit {
  showForm: any = false;
  showJson = false;
  dataForReplace: any;
  editIndex: any = -1;
  flag: any;
  obj: any;
  constructor(private formBuilder: FormBuilder) { }
  updatedData: any;
  sessionData = [{
    "firstName": "Dev", "lastName": "Datt", "city": "Allahabad"
  }, {
    "firstName": "Anshu", "lastName": "Mishra", "city": "Dewaria"
  }
  ];

  ngOnInit(): void {
    this.updatedData = this.sessionData;
    sessionStorage.setItem("sessionData", JSON.stringify(this.sessionData));
  }
  addItem(newItem: Object) {
    if (this.editIndex !== -1) {
      this.updatedData.splice(this.editIndex, 0, newItem);
      this.updatedData.splice(this.editIndex + 1, 1);
    }
  }
  edit(index) {
    this.obj = {
      "firstName": JSON.parse(sessionStorage.getItem("sessionData"))[index].firstName,
      "lastName": JSON.parse(sessionStorage.getItem("sessionData"))[index].lastName,
      "city": JSON.parse(sessionStorage.getItem("sessionData"))[index].city
    }
    this.editIndex = index;
    this.flag = 0;
    this.showForm = true;
  }
  addRow(newRow: Object) {
    this.updatedData.splice(this.updatedData.length, 0, newRow);
  }
  addUser() {
    this.showForm = true;
    this.flag = 1;
    this.obj = {
      "firstName": "",
      "lastName": "",
      "city": ""
    }
  }

  delete(index) {
    this.updatedData.splice(index, 1);
  }
  json() {
    this.showJson = true;
  }

}
