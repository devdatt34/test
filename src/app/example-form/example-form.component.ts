import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-example-form',
  templateUrl: './example-form.component.html',
  styleUrls: ['./example-form.component.scss']
})
export class ExampleFormComponent implements OnInit {
  exampleForm: FormGroup;
  exampleData: any = [];
  constructor(private fb: FormBuilder) {
    this.exampleForm = fb.group({
      title: new FormControl('', Validators.required),
      exampleSelect: new FormControl('', Validators.required),
      textArea: new FormControl('', Validators.required),
      check: new FormControl('', Validators.required),
      file: new FormControl()
    })
  }


  ngOnInit(): void {
  }

  submitForm() {
    var obj = {
      "title": this.exampleForm.controls.title.value,
      "selectedData": this.exampleForm.controls.exampleSelect.value,
      "textArea": this.exampleForm.controls.textArea.value,
      "isChecked": this.exampleForm.controls.check.value
    }
    this.exampleData.push(obj);
  }


}
