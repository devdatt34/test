import { CrudServiceService } from './../services/crud-service.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit, OnDestroy {

  createU = false;
  updateU = false;
  deleteU = false;

  userList: any = [];

  messages: any = [];
  subscription: Subscription;

  constructor(private _crudServices: CrudServiceService, private _messageService: MessageService) {
    this.subscription = this._messageService.onMessage().subscribe(message => {
      if (message) {
        this.messages.push(message);
        console.log(this.messages);
      } else {
        // clear messages when empty message received
        this.messages = [];
      }
    });
  }

  ngOnInit(): void {
    this._crudServices.getUsers().subscribe(data => {
      this.userList = data;
    })

  }

  addNewUser() {
    const newFormData = { id: 3, firstName: "John", lastName: "Snow", City: "Newyork" };
    this._crudServices.createUser(newFormData).subscribe(data => {
      console.log(data);
    });
    this.createU = true;
  }
  updateUser(userId) {
    const newFormData = { id: userId, firstName: "Raj", lastName: "Karan", City: "Delhi" };
    this._crudServices.updatedUser(userId, newFormData).toPromise();
    this.updateU = true;
  }
  deleteUser(userId) {
    this._crudServices.deletedUser(userId).toPromise();
    this.deleteU = true;
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }


}
