import { CrudServiceService } from './../services/crud-service.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {
  profileForm: FormGroup;
  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'email', message: 'please enter a valid email address.' }
    ]
  }


  constructor(private formBuilder: FormBuilder, private router: Router, private _crudService: CrudServiceService) {
    this.profileForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.email
      ])),
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required)
    })
  }


  ngOnInit(): void {
  }

  postProfile() {
    let obj = {
      "model": "UserManagement",
      "value": {
        "emailid": 'email data'
      },
      "changes": {
        "emailid": 'email data',
        "firstname": this.profileForm.controls.firstname.value,
        "lastname": this.profileForm.controls.lastname.value
      },
      "context": 1
    };
    let url = environment.base_url + environment.api_url.admin;
    let res = this._crudService.globalAPICall(obj, "put", url).toPromise();
    console.log(res);
    this.router.navigate(['dashboard/usercrud']);
  }
}
