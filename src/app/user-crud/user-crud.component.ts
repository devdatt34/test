import { CrudServiceService } from './../services/crud-service.service';
import { Component, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.scss']
})
export class UserCrudComponent implements OnInit {
  userData: any;
  constructor(
    private _crudService: CrudServiceService
  ) { }

  ngOnInit(): void {
    this.getUserDeatils();
  }

  async getUserDeatils() {
    let obj = { "list": "UserManagement", "connected": "false", "context": 1, "page": 1 };
    let url = environment.base_url + environment.api_url.admin;
    let res = await this._crudService.globalAPICall(obj, 'patch', url).toPromise();
    let body = JSON.parse(res.body);
    console.log(body.output);
    this.userData = body.output;
  }
  edit() {
    //will do after getting api
  }
  delete() {
    //will do after getting api
  }
}
