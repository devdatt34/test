import { AuthguardServiceService } from './../services/authguard-service.service';
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  showmsg = false;
  constructor(private _messageService: MessageService, private _authService: AuthguardServiceService) { }


  ngOnInit(): void {
  }

  sendMessage(): void {
    // send message to subscribers via observable subject
    this._messageService.sendMessage('Message from Dashboard to Crud Component!');
    this.showmsg = true;
  }

  clearMessages(): void {
    // clear messages
    this._messageService.clearMessages();
    this.showmsg = false;
  }
  logout() {
    this._authService.doLogout();
  }
}
