import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-array-and-string-manipulation',
  templateUrl: './array-and-string-manipulation.component.html',
  styleUrls: ['./array-and-string-manipulation.component.scss']
})
export class ArrayAndStringManipulationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  //String Variables
  txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  len = this.txt.length;
  str = "Please locate where 'locate' occurs!";
  pos = this.str.indexOf("locate");
  pos1 = this.str.search("locate");
  str1 = "Apple, Banana, Kiwi";
  res = this.str1.slice(7, 13);
  res1 = this.str1.substring(7, 13);
  res2 = this.str1.substr(7, 6);
  str2 = "Please visit Microsoft!";
  n = this.str2.replace("Microsoft", "Google");
  text1 = "Hello World!";
  text2 = this.text1.toUpperCase();
  text3 = this.text1.toLowerCase();
  t1 = "Hello";
  t2 = "World";
  t3 = this.t1.concat(" ", this.t2);
  str3 = "       Hello World!        ";
  res3 = this.str3.trim();
  txt1 = "a,b,c,d,e";

  // Array Variables
  fruits = ["Banana", "Orange", "Apple", "Mango"];
  popArr = this.fruits.pop();
  pushArr = this.fruits.push("Kiwi");
  shiftArr = this.fruits.shift();
  unShiftArr = this.fruits.unshift("Lemon");
  delValue = delete this.fruits[0];
  spliceArr = this.fruits.splice(2, 0, "Lemon", "Kiwi");
  remEle = this.fruits.splice(0, 1);
  citrus = this.fruits.slice(1);


}
