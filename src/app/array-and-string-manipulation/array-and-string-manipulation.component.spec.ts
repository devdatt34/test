import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrayAndStringManipulationComponent } from './array-and-string-manipulation.component';

describe('ArrayAndStringManipulationComponent', () => {
  let component: ArrayAndStringManipulationComponent;
  let fixture: ComponentFixture<ArrayAndStringManipulationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArrayAndStringManipulationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrayAndStringManipulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
