import { MessageService } from './services/message.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NovelDataComponent } from './novel-data/novel-data.component';
import { NovelListComponent } from './novel-list/novel-list.component';
import { ArrayAndStringManipulationComponent } from './array-and-string-manipulation/array-and-string-manipulation.component';
import { HttpClientModule } from '@angular/common/http';
import { EncrDecrService } from './services/encr-decr.service';
import { CrudComponent } from './crud/crud.component';
import { CrudwithsessionComponent } from './crudwithsession/crudwithsession.component';
import { AddeditComponent } from './addedit/addedit.component';
import { CustomPipe } from './pipe/custom.pipe';
import { ExampleFormComponent } from './example-form/example-form.component';
import { UserCrudComponent } from './user-crud/user-crud.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ProfileComponent,
    NovelDataComponent,
    NovelListComponent,
    ArrayAndStringManipulationComponent,
    CrudComponent,
    CrudwithsessionComponent,
    AddeditComponent,
    CustomPipe,
    ExampleFormComponent,
    UserCrudComponent,
    AdminProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    EncrDecrService, MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
