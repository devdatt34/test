import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-novel-list',
  templateUrl: './novel-list.component.html',
  styleUrls: ['./novel-list.component.scss']
})
export class NovelListComponent implements OnInit {
  @Input() items = [];

  constructor() { }

  ngOnInit(): void {
  }

}
